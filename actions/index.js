import { createStore, applyMiddleware } from 'redux';
import {thunk} from 'redux-thunk';


import { MY_EMAIL_ADDRESS_TYPE } from "../constants/index";

/* import axios from "axios";

const API_KEY = "6a78596d062df78380eff5944c4e5567";
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;
 */
export const FETCH_WEATHER = "FETCH_WEATHER";
export const FETCH_ADDRESS_LIST = "FETCH_ADDRESS_LIST";
export const DELETE_ADDRESS = "DELETE_ADDRESS";
export const ADD_ADDRESS = "ADD_ADDRESS";

/* export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER,
    payload: request
  };
} */
export function fetchAddressList(addressKind){
  // get list of Address objects of a specific kind - MyAddress or TheirAddress, from AsynStorage
  return {
    type: FETCH_ADDRESS_LIST,
    payload: AddressList 
  };
}
export function deleteAddress(EmailAddress){
  /* delete a single address
    EmailAddress object has two properties:
    - Address
    - Kind
  */
  return{
    type: DELETE_ADDRESS,
    payload: success //boolean
  }

}

export function addAddress(EmailAddress, addressKind){
  /* add a single address
    use addressKind to identify the table
    get a key from somewhere
  */
  const success = false; 
  if (addressKind === MY_EMAIL_ADDRESS_TYPE){
    const { myAddressList } = '';
    try{
      myAddressList = EmailAddress;
      this.setState({ MyAddressList: myAddressList });
  }
  catch(e){
    console.log(e);
    }
    try {
      myAddressList = this.state.MyAddressList.slice();
      myAddressList.push(EmailAddress);
      this.setState({ ...this.state.MyAddressList, ...myAddressList });
    }
    catch(e){
      console.log(e);
    }

    return{
      type: ADD_ADDRESS,
      payload: MyAddressList
    }
  }
  else
  { 
    const { theirAddressList } = '';
    if (!this.state.TheirAddressList){
      theirAddressList = this.state.TheirAddressList.slice();
      theirAddressList.push(EmailAddress);
    }
    else {
      theirAddressList = EmailAddress;
    }
    this.setState({ TheirAddressList: [...this.state.TheirAddressList, ...theirAddressList ] });
    return{
      type: ADD_ADDRESS,
      payload: TheirAddressList
    }
  }
}



const tintColor = '#2f95dc';
const LightGray = '#D3D3D3';
const LightBlue 	= '#ADD8E6';
const LightCoral 	= '#F08080';
const LightCyan 	= '#E0FFFF';	 
const LightGoldenRodYellow 	= '#FAFAD2';
const LightGrey 	= '#D3D3D3'	; 	
const LightGreen 	= '#90EE90'	; 	
const LightPink 	= '#FFB6C1'	; 	
const LightSalmon 	= '#FFA07A'	; 	
const LightSeaGreen 	= '#20B2AA'	; 	
const LightSkyBlue 	= '#87CEFA'	; 	
const LightSlateGray 	= '#778899';	 
const LightSlateGrey 	= '#778899'	; 	
const LightSteelBlue 	= '#B0C4DE'	; 	
const LightYellow 	= '#FFFFE0';


export default {
  LightGray ,
  LightBlue ,
  LightCoral 	,
  LightCyan 	,
  LightGoldenRodYellow ,
  LightGray ,	
  LightGrey 	,	
  LightGreen 	, 	
  LightPink 	 ,	
  LightSalmon ,	
  LightSeaGreen 	,	
  LightSkyBlue ,	
  LightSlateGray 	,
  LightSlateGrey 	, 	
  LightSteelBlue 	,
  LightYellow ,
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff'
};

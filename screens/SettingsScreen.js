import React from 'react';
import {connect} from 'react-redux';
import { Button, View, Text, FlatList, TextInput} from 'react-native';
import { ExpoConfigView } from '@expo/samples';
import EmailAddress from '../components/emailAddress';
//import { LightBlue, LightGray }  from './constants/Colors';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Settings',
  };
  constructor(props){
    super(props);
    this.state={
      MyAddressList: [],
      TheirAddressList: []
    };

  }

  onAddressList_Press(){
    //show Delete button
  }
  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <View >
        <EmailAddress />
        <FlatList 
          data={[{key: 'a'}, {key: 'b'}]}
          extraData = {this.state}
          renderItem={({item}) =>   
            <View style={{ margin: 10, padding:10 }}>
              <Text style={{  margin: 10, padding:10 }} >{item.key}</Text> 
              <Button 
                title='Delete'
                style={{ padding: 10, margin: 10 }} 
                onPress={this.onAddressList_Press.bind(this)} 
                color={'red'}
              />
            </View>
            
          } 
          />
          
        <ExpoConfigView />
      </View>
    );
  }
}

import React, { Component } from 'react';
import { View, TextInput, Text, Button, Switch } from 'react-native';
import { addAddress } from '../actions';
import { MY_EMAIL_ADDRESS_TYPE, THEIR_EMAIL_ADDRESS_TYPE } from '../constants';


class EmailAddress extends Component{
    constructor(props){
        super(props);
        this.state={ 
            text: 'address'
         };
         this.onAddAddress_Click = this.onAddAddress_Click.bind(this);
      }
      
      onAddAddress_Click(){
        //Add email to data  sbase
        // Add email to application state
        if (this.state.isMyEmailAddress){
            addAddress(this.state.text, MY_EMAIL_ADDRESS_TYPE);
        }
        else{
            addAddress(this.state.text, THEIR_EMAIL_ADDRESS_TYPE);
        }
      };

      render(){
        return (
            
            <View>
                <View>
                    <TextInput 
                        style={{height: 40,  borderWidth: 1, padding: 10, margin: 10 }}
                        autoCapitalize= {'none'}
                        autoCorrect={false}
                        spellCheck={false}
                        value={this.state.text}
                        onChangeText={({text}) => this.setState({text: text})}
                    />
                </View>
                <View style={{ flexDirection: 'row',justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={ { }} >My Email     </Text>
                    <Switch value= { this.state.isMyEmailAddress } onValueChange={ (value) =>  this.setState({ isMyEmailAddress: value }) }  /> 
                </View>
                <Button title='Add Address' onPress={this.onAddAddress_Click} />
            </View>
        );
      };


}

export default EmailAddress;